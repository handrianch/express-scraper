const router = require('express').Router()
const $ = require('cheerio')
const rp = require('request-promise')
const url = process.env.LINK_SCRAPE
const knex = require('../database')
const validate = require('express-validation')
const createError = require('http-errors')
const { KursQueryHelper, currencyFormatter } = require('../helpers')
const { KursTransformers } = require('../transformers')
const { kursValidation } = require('../validation')

router.get('/indexing', async (req, res, next) => {
    const kursData = []
    const date = new Date()
    const currentDate = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`

    try {
        const results = await knex('archive').count('id as length').where('date', currentDate).first()
        let message = 'Data has been indexed'

        if (results.length === 0) {
            const resultsHtml = await rp(url)
            const tableData = $('table.table tbody.text-right tr', resultsHtml)

            tableData.each((i, item) => {
                let rowsData = []

                $(item).find('td').each((j, td) => {
                    rowsData.push($(td).text())
                })

                kursData.push(rowsData)
            })

            for (item in kursData) {
                const kurs = kursData[item]
                let currency = await knex('currencies').where({ currency: kurs[0] }).first()

                if (!(currency)) {
                    currency = await knex('currencies').insert({ currency: kurs[0] })
                } else {
                    currency = [currency.id]
                }

                const e_rate = await knex('e_rate').insert({ jual: kurs[1], beli: kurs[2] })
                const tt_counter = await knex('tt_counter').insert({ jual: kurs[3], beli: kurs[4] })
                const bank_notes = await knex('bank_notes').insert({ jual: kurs[5], beli: kurs[6] })
                const archive = await knex('archive').insert({
                    currency: currency[0],
                    e_rate: e_rate[0],
                    tt_counter: tt_counter[0],
                    bank_notes: bank_notes[0],
                    date: currentDate
                })
            }

            message = 'Data successfully indexed'
        }

        return res.status(200).json({ message })
    } catch (e) {
        return next(createError(500))
    }
})

router.get('/kurs', validate(kursValidation.dateRange), async (req, res, next) => {
    const { startdate, enddate } = req.query

    try {
      const results = await KursQueryHelper.getKursWithDateRange(startdate, enddate)

      const collections = new KursTransformers(results)
      return res.status(200).json(collections)
    } catch(e) {
      return next(createError(500))
    }
})

router.get('/kurs/:symbol', validate(kursValidation.symbolParams), async (req, res, next) => {
    const { symbol } = req.params
    const { startdate, enddate } = req.query

    try {
        const results = await KursQueryHelper.getKursWithSymbolDateRange(symbol, startdate, enddate)
        const collections = new KursTransformers(results)
        return res.status(200).json(collections)
    } catch(e) {
      return next(createError(500))
    }
})

router.post('/kurs', validate(kursValidation.bodyRequest), async (req, res, next) => {
    const body = req.body

    try {
        let currency = await knex('currencies').where('currency', body.symbol).first()

        if (!currency) {
            currency = await knex('currencies').insert({ currency: body.symbol.toUpperCase() })
        } else {
            currency = [currency.id]
        }

        const { archiveExists } = await knex('archive')
                                            .count('id as archiveExists')
                                            .where('currency', currency)
                                            .where('date', body.date)
                                            .first()

        if(archiveExists) {
            return next(createError(409, "Duplication data"))
        }

        const e_rate = await knex('e_rate').insert(currencyFormatter(body.e_rate))
        const tt_counter = await knex('tt_counter').insert(currencyFormatter(body.tt_counter))
        const bank_notes = await knex('bank_notes').insert(currencyFormatter(body.bank_notes))
        const archive = await knex('archive').insert({
            currency: currency[0],
            e_rate: e_rate[0],
            tt_counter: tt_counter[0],
            bank_notes: bank_notes[0],
            date: body.date
        })

        const results = await KursQueryHelper.getKursWithArchiveId(archive[0])
        const collections = new KursTransformers(results)
        return res.status(201).json(collections[0])
    } catch (e) {
        console.log(e)
        return next(createError(500))
    }
})

router.put('/kurs', validate(kursValidation.bodyRequest), async (req, res, next) => {
    const body = req.body

    try {
        const kursArchive = await knex('archive')
                                    .where('currency', function() {
                                        this.select('id').from('currencies').where('currency', body.symbol).first()
                                     })
                                    .where('date', body.date)
                                    .first()

        if(!kursArchive) {
            return next(createError(404))
        }

        const e_rate = await knex('e_rate')
                                .where('id', kursArchive.e_rate)
                                .update(currencyFormatter(body.e_rate))

        const tt_counter = await knex('tt_counter')
                                    .where('id', kursArchive.tt_counter)
                                    .update(currencyFormatter(body.tt_counter))

        const bank_notes = await knex('bank_notes')
                                    .where('id', kursArchive.bank_notes)
                                    .update(currencyFormatter(body.bank_notes))

        const results = await KursQueryHelper.getKursWithArchiveId(kursArchive.id)

        const collections = new KursTransformers(results)
        return res.status(200).json(collections[0])
    } catch(e) {
        return next(createError(500))
    }
})

router.delete('/kurs/:date', validate(kursValidation.dateParams), async (req, res, next) => {
    const date = req.params.date

    try {
        const results = await KursQueryHelper.deleteKursFromDate(date)

        if (results[0].affectedRows === 0) {
            return next(createError(404))
        }

        return res.status(204).json()
    } catch (e) {
        return next(createError(500))
    }
})


module.exports = router