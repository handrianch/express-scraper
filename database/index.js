const config = require('../config/dbConfig')

const knex = require('knex')({
  client: 'mysql',
  connection: {
    host: config.host,
    user: config.user,
    password: config.password,
    database: config.database,
    timezone: 'UTC',
    typeCast: function(field, next) {
      if(field.type === 'DATE') {
        const date = new Date(field.string())
        const trueMonth = date.getMonth() + 1
        const month = trueMonth < 10 ? `0${trueMonth}` : `${trueMonth}`
        return `${date.getFullYear()}-${month}-${date.getDate()}`
      }
      return next()
    }
  }
})

module.exports = knex