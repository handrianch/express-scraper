const knex = require('../database')

class KursQueryHelper {
  constructor() {
    this.select = [
        'currencies.currency',
        'e_rate.jual', 'e_rate.beli',
        'tt_counter.jual', 'tt_counter.beli',
        'bank_notes.jual', 'bank_notes.beli',
        'archive.date',
    ]
  }

  async getKursWithDateRange(startdate, enddate) {
    const results = await knex('archive')
                                .select(this.select)
                                .innerJoin('currencies', 'archive.currency', 'currencies.id')
                                .innerJoin('e_rate', 'archive.e_rate', 'e_rate.id')
                                .innerJoin('tt_counter', 'archive.tt_counter', 'tt_counter.id')
                                .innerJoin('bank_notes', 'archive.bank_notes', 'bank_notes.id')
                                .where('archive.date', '>=', startdate)
                                .where('archive.date', '<=', enddate)
                                .orderBy([ { column: 'archive.date' }, { column: 'currencies.id' } ])
                                .options({ nestTables: true })
    return results
  }

  async getKursWithSymbolDateRange(symbol, startdate, enddate) {
    const results = await knex('archive')
                              .select(this.select)
                              .innerJoin('currencies', 'archive.currency', 'currencies.id')
                              .innerJoin('e_rate', 'archive.e_rate', 'e_rate.id')
                              .innerJoin('tt_counter', 'archive.tt_counter', 'tt_counter.id')
                              .innerJoin('bank_notes', 'archive.bank_notes', 'bank_notes.id')
                              .where('currencies.currency', function() {
                                this.select('currency').from('currencies').where('currency', symbol).first()
                              })
                              .where('archive.date', '>=', startdate)
                              .where('archive.date', '<=', enddate)
                              .orderBy([ { column: 'archive.date' }, { column: 'currencies.id' } ])
                              .options({ nestTables: true })
    return results
  }

  async getKursWithArchiveId(id) {
    const results = await knex('archive')
                            .select(this.select)
                            .innerJoin('currencies', 'archive.currency', 'currencies.id')
                            .innerJoin('e_rate', 'archive.e_rate', 'e_rate.id')
                            .innerJoin('tt_counter', 'archive.tt_counter', 'tt_counter.id')
                            .innerJoin('bank_notes', 'archive.bank_notes', 'bank_notes.id')
                            .where('archive.id', id)
                            .options({
                              nestTables: true
                            })
    return results
  }

  async deleteKursFromDate(date) {
    const results = await knex.raw(`
          DELETE e_rate, tt_counter, bank_notes
          FROM archive
          INNER JOIN e_rate ON archive.e_rate = e_rate.id
          INNER JOIN tt_counter ON archive.tt_counter = tt_counter.id
          INNER JOIN bank_notes ON archive.bank_notes = bank_notes.id
          WHERE archive.date = ?
        `, [date])

    return results
  }
}

module.exports = new KursQueryHelper()