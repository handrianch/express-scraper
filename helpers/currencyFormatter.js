const convertToCurrency = require('./currencyConverter')

function currencyFormatter(obj) {
  return {
    jual: convertToCurrency(obj.jual),
    beli: convertToCurrency(obj.beli),
  }
}

module.exports = currencyFormatter