function convertToCurrency(param) {
  return param.toFixed(2)
              .replace('.', ',')
              .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}

module.exports = convertToCurrency