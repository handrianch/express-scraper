const supertest = require('supertest')
const should = require('should')
const app = require('../app')
const server = supertest.agent(app)
const knex = require('../database')
const { currencyConverter } = require('../helpers')

const date = new Date()
const trueMonth = date.getMonth() + 1
const month = trueMonth < 10 ? `0${trueMonth}` : `${trueMonth}`
const startdate =`${date.getFullYear()}-${month}-${date.getDate()}`

const endmonth = trueMonth + 1
const enddate =`${date.getFullYear()}-${endmonth < 10 ? `0${endmonth}` : `${endmonth}`}-${date.getDate()}`

describe("Testing API", function() {
  before(async function() {
    await knex.raw(`SET FOREIGN_KEY_CHECKS = 0`)
    await knex('archive').truncate()
    await knex('currencies').truncate()
    await knex('e_rate').truncate()
    await knex('tt_counter').truncate()
    await knex('bank_notes').truncate()
    await knex.raw(`SET FOREIGN_KEY_CHECKS = 1`)
  })

  describe("scraping data from outside", function() {
    it("storing data to storage and got response message successfully indexing", function(done) {
      server.get("/api/indexing")
            .expect("Content-Type", /json/)
            .expect(200)
            .expect("Data successfully indexed")
            .end(function(err, res) {
              res.status.should.equal(200)
              res.body.message.should.equal("Data successfully indexed")
              done()
            })
    })

    it("checking storage for existing data and got response message indexed", function(done) {
      server.get("/api/indexing")
            .expect("Content-Type", /text\/html/)
            .expect(200)
            .expect("Data has been indexed")
            .end(function(err, res) {
              res.status.should.equal(200)
              res.body.message.should.equal("Data has been indexed")
              done()
            })
    })
  })

  describe("storing data to storage from input user", function() {
    it("should store data and return that data", function(done) {
      const payload = {
        symbol: 'ASD',
        e_rate: {
          jual: 1111.55,
          beli: 2222
        },
        tt_counter: {
          jual: 3333.55,
          beli: 4444
        },
        bank_notes: {
          jual: 5555.55,
          beli: 666
        },
        date: enddate
      }

      const response = {
        symbol: 'ASD',
        e_rate: {
          jual: currencyConverter(1111.55),
          beli: currencyConverter(2222)
        },
        tt_counter: {
          jual: currencyConverter(3333.55),
          beli: currencyConverter(4444)
        },
        bank_notes: {
          jual: currencyConverter(5555.55),
          beli: currencyConverter(666)
        },
        date: enddate
      }

      server.post("/api/kurs")
            .send(payload)
            .set('Accept', 'applicaton/json')
            .expect('Content-Type', /json/)
            .expect(201, response, done)
    })
  })

  describe("update data from storage where input is obtained from the user", function() {
    it("should update data and return that data", function(done) {
      const payload = {
        symbol: 'ASD',
        e_rate: {
          jual: 9999.55,
          beli: 4444
        },
        tt_counter: {
          jual: 8767.55,
          beli: 3333
        },
        bank_notes: {
          jual: 4657.55,
          beli: 353
        },
        date: enddate
      }

      const response = {
        symbol: 'ASD',
        e_rate: {
          jual: currencyConverter(9999.55),
          beli: currencyConverter(4444)
        },
        tt_counter: {
          jual: currencyConverter(8767.55),
          beli: currencyConverter(3333)
        },
        bank_notes: {
          jual: currencyConverter(4657.55),
          beli: currencyConverter(353)
        },
        date: enddate
      }

      server.put('/api/kurs')
            .send(payload)
            .set('Accept', 'applicaton/json')
            .expect('Content-Type', /json/)
            .expect(200, response, done)
    })
  })

  describe("retrieving data from storage", function() {
    it("should retrieve data from date range", function(done) {
      server.get(`/api/kurs?startdate=${startdate}&enddate=${enddate}`)
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function(err, res) {
              res.status.should.equal(200)
              res.body.should.containDeep([
                {"symbol": "ASD", "e_rate": {"jual" : currencyConverter(9999.55)}},
                {"symbol" : "USD"},
                {"date" : startdate}
              ])
              done()
            })
    })

    it("should retrieve data from symbol and date range", function(done) {
      const symbol = "USD"
      server.get(`/api/kurs/${symbol}?startdate=${startdate}&enddate=${enddate}`)
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function(err, res) {
              res.status.should.equal(200)
              res.body.should.containDeep([
                {"symbol" : symbol},
              ])
              res.body.should.not.containDeep([{"symbol" : "UIO"}])
              done()
            })
    })
  })

  describe("remove data from storage", function() {
    it("harus menghapus semua data dari penyimpanan menurut tanggal", function(done) {
      server.delete(`/api/kurs/${enddate}`)
            .expect("Content-Type", "applicaton/json")
            .expect(204)
            .end(function(err, res) {
              res.status.should.equal(204)
              res.body.should.be.empty()
              done()
            })
    })
  })

  after(async function() {
    await knex.raw(`SET FOREIGN_KEY_CHECKS = 0`)
    await knex('archive').truncate()
    await knex('currencies').truncate()
    await knex('e_rate').truncate()
    await knex('tt_counter').truncate()
    await knex('bank_notes').truncate()
    await knex.raw(`SET FOREIGN_KEY_CHECKS = 1`)
  })
})