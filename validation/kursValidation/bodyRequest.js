const Joi = require('@hapi/joi')

module.exports = {
  body : {
    symbol: Joi.string().min(3).max(3).required(),
    date: Joi.date().iso().required(),
    e_rate: Joi.object().keys({ jual: Joi.number().required(), beli: Joi.number().required()}).required(),
    tt_counter: Joi.object().keys({ jual: Joi.number().required(), beli: Joi.number().required()}).required(),
    bank_notes: Joi.object().keys({ jual: Joi.number().required(), beli: Joi.number().required()}).required(),
  }
}