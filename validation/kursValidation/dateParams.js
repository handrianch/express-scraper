const Joi = require('@hapi/joi')

module.exports = {
  params: {
    date: Joi.date().iso().required()
  }
}