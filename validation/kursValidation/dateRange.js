const Joi = require('@hapi/joi')

module.exports = {
  query: {
    startdate: Joi.date().iso().required(),
    enddate: Joi.date().iso().required(),
  }
}