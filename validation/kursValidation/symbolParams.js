const Joi = require('@hapi/joi')

module.exports = {
  params: {
    symbol: Joi.string().uppercase({convert: false}).length(3).required()
  },
  query: {
    startdate: Joi.date().iso().required(),
    enddate: Joi.date().iso().required()
  }
}