-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 26, 2019 at 06:48 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kurs_bca`
--

-- --------------------------------------------------------

--
-- Table structure for table `archive`
--

CREATE TABLE `archive` (
  `id` int(11) NOT NULL,
  `currency` int(2) NOT NULL,
  `e_rate` int(11) NOT NULL,
  `tt_counter` int(11) NOT NULL,
  `bank_notes` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `archive`
--

INSERT INTO `archive` (`id`, `currency`, `e_rate`, `tt_counter`, `bank_notes`, `date`) VALUES
(1, 1, 1, 1, 1, '2019-07-26'),
(2, 2, 2, 2, 2, '2019-07-26'),
(3, 3, 3, 3, 3, '2019-07-26'),
(4, 4, 4, 4, 4, '2019-07-26'),
(5, 5, 5, 5, 5, '2019-07-26'),
(6, 6, 6, 6, 6, '2019-07-26'),
(7, 7, 7, 7, 7, '2019-07-26'),
(8, 8, 8, 8, 8, '2019-07-26'),
(9, 9, 9, 9, 9, '2019-07-26'),
(10, 10, 10, 10, 10, '2019-07-26'),
(11, 11, 11, 11, 11, '2019-07-26'),
(12, 12, 12, 12, 12, '2019-07-26'),
(13, 13, 13, 13, 13, '2019-07-26'),
(14, 14, 14, 14, 14, '2019-07-26'),
(15, 15, 15, 15, 15, '2019-07-26'),
(16, 16, 16, 16, 16, '2019-07-26');

-- --------------------------------------------------------

--
-- Table structure for table `bank_notes`
--

CREATE TABLE `bank_notes` (
  `id` int(11) NOT NULL,
  `jual` varchar(10) COLLATE utf8_bin NOT NULL,
  `beli` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `bank_notes`
--

INSERT INTO `bank_notes` (`id`, `jual`, `beli`) VALUES
(1, '14.125,00', '13.825,00'),
(2, '10.333,00', '10.106,00'),
(3, '15.784,00', '15.372,00'),
(4, '9.849,00', '9.575,00'),
(5, '2.148,00', '2.018,00'),
(6, '1.536,00', '1.418,00'),
(7, '10.767,00', '10.463,00'),
(8, '14.312,00', '13.924,00'),
(9, '9.438,00', '9.168,00'),
(10, '17.627,00', '17.184,00'),
(11, '1.822,00', '1.753,00'),
(12, '132,04', '125,42'),
(13, '3.806,00', '3.636,00'),
(14, '2.093,00', '1.966,00'),
(15, '0,00', '0,00'),
(16, '460,00', '431,00');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(2) NOT NULL,
  `currency` varchar(3) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `currency`) VALUES
(1, 'USD'),
(2, 'SGD'),
(3, 'EUR'),
(4, 'AUD'),
(5, 'DKK'),
(6, 'SEK'),
(7, 'CAD'),
(8, 'CHF'),
(9, 'NZD'),
(10, 'GBP'),
(11, 'HKD'),
(12, 'JPY'),
(13, 'SAR'),
(14, 'CNH'),
(15, 'MYR'),
(16, 'THB');

-- --------------------------------------------------------

--
-- Table structure for table `e_rate`
--

CREATE TABLE `e_rate` (
  `id` int(11) NOT NULL,
  `jual` varchar(10) COLLATE utf8_bin NOT NULL,
  `beli` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `e_rate`
--

INSERT INTO `e_rate` (`id`, `jual`, `beli`) VALUES
(1, '14.009,00', '13.989,00'),
(2, '10.253,06', '10.205,06'),
(3, '15.641,80', '15.541,80'),
(4, '9.741,30', '9.661,30'),
(5, '2.118,14', '2.058,14'),
(6, '1.496,79', '1.456,79'),
(7, '10.659,74', '10.579,74'),
(8, '14.165,75', '14.065,75'),
(9, '9.345,10', '9.265,10'),
(10, '17.462,50', '17.362,50'),
(11, '1.806,16', '1.776,16'),
(12, '130,52', '127,12'),
(13, '3.772,93', '3.692,93'),
(14, '2.094,69', '1.974,69'),
(15, '3.438,06', '3.358,06'),
(16, '456,27', '448,27');

-- --------------------------------------------------------

--
-- Table structure for table `tt_counter`
--

CREATE TABLE `tt_counter` (
  `id` int(11) NOT NULL,
  `jual` varchar(10) COLLATE utf8_bin NOT NULL,
  `beli` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tt_counter`
--

INSERT INTO `tt_counter` (`id`, `jual`, `beli`) VALUES
(1, '14.150,00', '13.850,00'),
(2, '10.270,10', '10.189,10'),
(3, '15.787,45', '15.410,45'),
(4, '9.833,85', '9.570,85'),
(5, '2.126,05', '2.060,05'),
(6, '1.505,80', '1.453,40'),
(7, '10.752,10', '10.483,10'),
(8, '14.297,15', '13.952,15'),
(9, '9.443,70', '9.160,70'),
(10, '17.643,95', '17.185,95'),
(11, '1.808,15', '1.774,15'),
(12, '131,31', '126,45'),
(13, '3.785,65', '3.680,65'),
(14, '2.118,05', '1.952,95'),
(15, '3.446,10', '3.345,10'),
(16, '457,25', '447,25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `archive`
--
ALTER TABLE `archive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `currency.id.archive` (`currency`),
  ADD KEY `e_rate.id.archive` (`e_rate`),
  ADD KEY `bank_notes.id.archive` (`bank_notes`),
  ADD KEY `tt_counter.id.archive` (`tt_counter`) USING BTREE;

--
-- Indexes for table `bank_notes`
--
ALTER TABLE `bank_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `e_rate`
--
ALTER TABLE `e_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tt_counter`
--
ALTER TABLE `tt_counter`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `archive`
--
ALTER TABLE `archive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `bank_notes`
--
ALTER TABLE `bank_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `e_rate`
--
ALTER TABLE `e_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tt_counter`
--
ALTER TABLE `tt_counter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `archive`
--
ALTER TABLE `archive`
  ADD CONSTRAINT `bank_notes.id.archive` FOREIGN KEY (`bank_notes`) REFERENCES `bank_notes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `currency.id.archive` FOREIGN KEY (`currency`) REFERENCES `currencies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `e_rate.id.archive` FOREIGN KEY (`e_rate`) REFERENCES `e_rate` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tt_rate.id.archive` FOREIGN KEY (`tt_counter`) REFERENCES `tt_counter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
