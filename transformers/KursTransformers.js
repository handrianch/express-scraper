
class KursTransformers {
  constructor(data) {
    this.data = data

    return this.data.map(item => {
      return this.transform(item)
    })
  }

  transform(collection) {
    return {
      symbol: collection.currencies.currency,
      e_rate: collection.e_rate,
      tt_counter: collection.tt_counter,
      bank_notes: collection.bank_notes,
      date: collection.archive.date
    }
  }
}

module.exports = KursTransformers